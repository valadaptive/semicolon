# semicolon

semicolon is a Discord bot that focuses on simple moderation and user tools.

## Commands

_Sorry, but this has yet to be written! In the mean time, please check out `;help`._

## Localization

semicolon fully supports localization, from running commands to giving output! Check out [langs/en_us.json](https://gitlab.com/lexikiq/semicolon/-/blob/master/langs/en_us.json) to get started and ask if you need any help :)

As of right now, the bot has unfortunately not been translated into any languages.