import math
import re

import discord
from discord.ext import commands

from cogs.utils.bot import Semicolon


async def is_lexi_lounge(ctx):
    return ctx.guild and ctx.guild.id == 179017865779871744


class SequenceNotFound(Exception):
    pass


async def msg_search(channel: discord.TextChannel, regex, limit=1):
    results = []
    iterations = 0
    async for msg in channel.history():
        iterations += 1
        rematch = regex.match(msg.content)
        if rematch:
            if rematch.groups():
                results.append(rematch.groups())
            else:
                results.append(rematch.group(0))
            if len(results) >= limit:
                if limit == 1:
                    results = results[0]
                return results, iterations
    return None, None


class Lounge(commands.Cog):
    def __init__(self, bot: Semicolon):
        self.bot = bot
        self.funcs = [self.polls, self.op, self.eyes, self.counting, self.fibonacci, self.unicode, self.powers_of_two, self.xkcd_counting]

    @commands.command(hidden=True)
    @commands.check(is_lexi_lounge)
    async def next(self, ctx, channel: discord.TextChannel):
        """[LCL] Posts the next expected message of a fun&games channel"""
        func = next((x for x in self.funcs if x.__name__ == channel.name), None)
        if func is not None:
            try:
                next_var = await func(channel)
            except SequenceNotFound:
                next_var = self.bot.i18n.localize('error', ctx=ctx)+self.bot.i18n.localize('lcl_not_found', ctx=ctx)
        else:
            next_var = self.bot.i18n.localize('lcl_channel_missing', ctx=ctx)
        await ctx.send(next_var)

    async def polls(self, channel: discord.TextChannel):
        return self.bot.i18n.localize('lcl_polls', guild=channel.guild)

    async def op(self, channel: discord.TextChannel):
        return 'op'

    async def eyes(self, channel: discord.TextChannel):
        return '\N{EYES}'

    async def counting(self, channel: discord.TextChannel):
        next_var, iterations = await msg_search(channel, re.compile(r'\d+'))
        if next_var:
            return int(next_var) + iterations
        raise SequenceNotFound(self.bot.i18n.localize('lcl_no_next_item'))

    async def fibonacci(self, channel: discord.TextChannel):
        next_vars, iterations = await msg_search(channel, re.compile(r'\d+'), limit=2)
        if next_vars:
            ints = list(map(int, next_vars))[::-1]
            sums = sum(ints)
            while iterations > 2:
                ints[0] = ints[1]
                ints[1] = sums
                sums = sum(ints)
                iterations -= 1
            return sums
        raise SequenceNotFound(self.bot.i18n.localize('lcl_no_next_item'))

    async def unicode(self, channel: discord.TextChannel):
        next_var, iterations = await msg_search(channel, re.compile(r'(.) \(U\+[\dABCDEF]{4}\)'), limit=1)
        if next_var:
            ordinance = ord(next_var[0]) + iterations
            return '{0} (U+{1:0>4})'.format(chr(ordinance), hex(ordinance)[2:].upper())
        raise SequenceNotFound(self.bot.i18n.localize('lcl_no_next_item'))

    async def powers_of_two(self, channel: discord.TextChannel):
        next_var, iterations = await msg_search(channel, re.compile(r'\d+'), limit=1)
        if next_var:
            next_var = int(next_var) * int(math.pow(2, iterations))
            return '{} (2^{})'.format(next_var, int(math.log2(next_var)))
        raise SequenceNotFound(self.bot.i18n.localize('lcl_no_next_item'))

    async def xkcd_counting(self, channel: discord.TextChannel):
        next_var, iterations = await msg_search(channel, re.compile(r'https?://(?:www\.)?xkcd\.com/(\d+)'), limit=1)
        if next_var:
            next_var = int(next_var[0]) + iterations
            return '<https://xkcd.com/{}/>'.format(next_var)
        raise SequenceNotFound(self.bot.i18n.localize('lcl_no_next_item'))



def setup(bot):
    bot.add_cog(Lounge(bot))
