import os
import random
import re
import typing
from io import BytesIO

import discord
from PIL import Image
from aiohttp import ClientSession
from discord.ext import commands

from cogs.utils.bot import Semicolon
from cogs.utils.constants import CONSTANTS
from cogs.utils.errors import WebException
from cogs.utils.utils import JsonManager, embed_author_template


class PokemonData:
    __slots__ = {"name", "dex", "sprite", "error"}

    def __init__(self, name: str, dex: int, sprite: typing.Union[str, None], error: str = None):
        self.name = name
        self.dex = dex
        self.sprite = sprite
        self.error = error


class PokemonManager(JsonManager):
    __slots__ = {"session", "i18n"}

    def __init__(self, session: ClientSession, localizer):
        super().__init__('pokemon.json')
        self.session = session
        self.i18n = localizer
        os.makedirs(CONSTANTS['pkmn_sprites_dir'], exist_ok=True)

    def init_data(self, key: str, init_with=None):
        if init_with is None:
            init_with = []
        super().init_data(key, init_with)

    async def pkmn(self, ctx: commands.Context, pokemon: typing.Union[str, int], endpoint: str = 'pokemon') -> PokemonData:
        async with self.session.get(f"https://pokeapi.co/api/v2/{endpoint}/{pokemon}") as r:
            if r.status == 200:
                data = await r.json()
            else:
                raise WebException(self.i18n, ctx, 'PokéAPI', r.status, await r.text())
        return PokemonData(data['name'].title(), data['id'], data['sprites']['front_default'])

    async def set_sprite(self, embed: discord.Embed, poke_data: PokemonData) -> tuple:
        output_name = 'poke{}.png'.format(poke_data.dex)
        savedir = CONSTANTS['pkmn_sprites_dir']
        output_path = os.path.join(savedir, output_name)
        file = None
        if poke_data.sprite:
            if output_name not in os.listdir(savedir):
                async with self.session.get(poke_data.sprite) as r:
                    if r.status != 200:
                        return embed, None
                    sprite_data = BytesIO(await r.read())
                sprite: Image.Image = Image.open(sprite_data)
                upscale = 3
                sprite = sprite.resize((sprite.width*upscale, sprite.height*upscale), Image.NEAREST)
                # P -> RGBA -> RGBa (fixes the issue of non-black transparent pixels not cropping) -> get mask of image
                bbox = sprite.convert(mode='RGBA').convert(mode='RGBa').getbbox()
                sprite.crop(bbox).save(output_path)
            file = discord.File(output_path, filename=output_name)
            embed.set_image(url="attachment://" + output_name)
        return embed, file

    async def get_info(self, ctx: commands.Context, pokemon: str):
        poke_data = await self.pkmn(ctx, pokemon)
        embed = embed_author_template(ctx, subtle_author=None)
        embed.title = self.i18n.localize('pokemon_name', ctx=ctx).format(poke_data)
        embed.colour = discord.Colour(0xF93D32)
        embed, file = await self.set_sprite(embed, poke_data)
        if file is None:
            embed.description = self.i18n.localize('pokemon_no_sprite', ctx=ctx)

        kwargs = {'embed': embed}
        if file is not None:
            kwargs['file'] = file

        await ctx.send(file=file, embed=embed)

    async def catch(self, ctx):
        user_key = str(ctx.author.id)
        self.init_data(user_key)

        poke_data = await self.pkmn(ctx, random.randint(1, CONSTANTS['max_pokemon']))
        embed = embed_author_template(ctx)
        embed, file = await self.set_sprite(embed, poke_data)
        embed.title = self.i18n.localize('pokemon_catch', ctx=ctx).format(self.i18n.localize('pokemon_name', ctx=ctx)
                                                                          .format(poke_data))
        if poke_data.dex not in self.data[user_key]:
            self.data[user_key].append(poke_data.dex)
            self.save()
            desc = self.i18n.localize('pokemon_new', ctx=ctx)
            clr = discord.Colour.green()
        else:
            desc = self.i18n.localize('pokemon_old', ctx=ctx)
            clr = discord.Colour.dark_green()
        embed.description = desc
        embed.colour = clr

        kwargs = {'embed': embed}
        if file is not None:
            kwargs['file'] = file

        await ctx.send(**kwargs)

    def import_data(self):
        old_data_path = '../pkmn'
        for filename in (f for f in os.listdir(old_data_path) if re.match(r"\d{17,}\.txt", f)):
            filename_key = filename.replace('.txt', '')
            self.init_data(filename_key)
            userlist = self.data[filename_key]
            with open(os.path.join(old_data_path, filename), 'r') as f:
                dex_ids = f.readlines()
                for dex_id in dex_ids:
                    dex_id = int(dex_id)
                    if dex_id not in userlist:
                        userlist.append(dex_id)
        self.save()


class PokemonCog(commands.Cog, name='Pokemon'):
    def __init__(self, bot: Semicolon):
        self.bot = bot
        self.pokemon_manager = PokemonManager(bot.session, bot.i18n)

    @commands.group(invoke_without_command=True, name='pokémon', aliases=['pkmn', 'pokemon', 'poke', 'poké'])
    @commands.cooldown(rate=1, per=3, type=commands.BucketType.channel)
    @commands.bot_has_permissions(embed_links=True)
    async def pokemon(self, ctx: commands.Context, pokemon: str):
        """Get the information of a specified Pokémon.
        Accepts IDs or names.
        Powered by pokeapi.co."""
        async with ctx.typing():
            await self.pokemon_manager.get_info(ctx, re.sub(r"[^\w\d]", '', pokemon.lower()))

    @pokemon.group(name='catch', invoke_without_command=True)
    @commands.cooldown(rate=1, per=600, type=commands.BucketType.user)
    @commands.bot_has_permissions(embed_links=True)
    async def pokemon_catch(self, ctx: commands.Context):
        """Catch a Pokémon!"""
        async with ctx.typing():
            await self.pokemon_manager.catch(ctx)

    @pokemon_catch.command(name='top', hidden=True)
    async def pokemon_catch_top(self, ctx: commands.Context):
        await ctx.send(ctx.bot.i18n.localize("unimplemented", ctx=ctx), delete_after=5)

    @pokemon.command(name='import')
    @commands.is_owner()
    async def pokemon_import(self, ctx: commands.Context):
        self.pokemon_manager.import_data()
        await ctx.send(self.bot.i18n.localize('imported', ctx=ctx))


def setup(bot):
    bot.add_cog(PokemonCog(bot))
