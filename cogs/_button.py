import datetime
import json
import math
import os
import traceback

# import asyncpg
import discord
from colour import Color
from discord.ext import tasks, commands

from cogs.utils.bot import Semicolon
from cogs.utils.utils import Localizer, comma_separator, data_path

default_loop_interval = 2.0
time_output = "%m/%d/%Y %H:%M:%S"


def time_format(seconds, localizer: Localizer, raw_output=False):
    weeks, days = divmod(seconds, 60 * 60 * 24 * 7)
    days, hours = divmod(days, 60 * 60 * 24)
    hours, minutes = divmod(hours, 60 * 60)
    minutes, seconds = divmod(minutes, 60)
    if raw_output:
        return weeks, days, hours, minutes, seconds
    weeks, days, hours, minutes = map(round, [weeks, days, hours, minutes])  # lol
    seconds = round(seconds, 2)  # stupid code
    output = []
    if weeks > 0: output.append(localizer.localize("time_weeks").format(weeks))
    if days > 0: output.append(localizer.localize("time_days").format(days))
    if hours > 0: output.append(localizer.localize("time_hours").format(hours))
    if minutes > 0: output.append(localizer.localize("time_minutes").format(minutes))
    if seconds > 0: output.append(localizer.localize("time_seconds").format(seconds))
    return comma_separator(output)


class Button(commands.Cog):
    def __init__(self, bot: Semicolon):
        self.bot = bot
        self.button_file = data_path('button.json')
        self.button_json = None
        self.the_message = None
        self.gradient_lst = None
        if not os.path.isfile(self.button_file):
            # button.json not found, must be first initialization
            self.button_json = json.loads('{}')
            self.button_json['work'] = False  # if cog should work
            self.button_json['initial_timer'] = 60*15  # 60*minutes
            self.button_json['channel_id'] = 383736513994686475  # where to put the timer
            self.button_json['BTN'] = '🔘'  # the button emoji
            self.button_json['pressers'] = 0  # how many people have pressed the button
            self.start = datetime.datetime.now()  # when the game started (for game over msg)
            self.end_game_at = self.start + datetime.timedelta(seconds=self.button_json['initial_timer'])  # when to end
            self.write_json()  # save json to disk
        else:
            with open(self.button_file, 'r') as f:
                self.button_json = json.load(f)
            self.start = datetime.datetime.strptime(self.button_json['start'], time_output)
            self.end_game_at = datetime.datetime.strptime(self.button_json['end_game_at'], time_output)

        if self.button_json['work']:
            # self.write_message.add_exception_type(asyncpg.PostgresConnectionError)
            self.write_message.start()

    def cog_unload(self):
        self.write_message.cancel()

    def write_json(self):
        """
        Saves json to disk
        """
        self.button_json['start'] = self.start.strftime(time_output)
        self.button_json['end_game_at'] = self.end_game_at.strftime(time_output)
        with open(self.button_file, 'w') as f:
            json.dump(self.button_json, f)

    def get_color(self, delta: float):
        """
        Gets the color corresponding to the time left on the timer
        :param delta: time left on the timer
        :return: a discord.Colour object
        """
        delta = min(math.floor(delta), 60)
        divisors = 6
        count_const = 60 / divisors
        count = round(count_const * math.floor(delta / count_const))
        role_name = f"{count}s+"
        role_clr = int(count / count_const)
        if self.gradient_lst is None:  # don't want to waste resources recalculating this every time
            self.gradient_lst = list(Color("#ED2525").range_to(Color("#F4D58B"), divisors + 1))
        disc_clr = discord.Colour(int(self.gradient_lst[role_clr].get_hex_l()[1:], 16))
        return disc_clr, role_name

    async def add_role(self, mmbr: discord.Member, delta: float):
        """
        Adds (and creates) a Discord role showing the button timer as of pressing The Button
        :param mmbr: discord.Member object
        :param delta: seconds left on the timer
        """
        if not mmbr.guild.me.guild_permissions.manage_roles:
            self.bot.logger.warning("The Button: Missing manage roles")
            return
        discclr, role_name = self.get_color(delta)
        if role_name not in (role.name for role in mmbr.guild.roles):
            user_role = await mmbr.guild.create_role(name=role_name, color=discclr)
        else:
            user_role = discord.utils.get(mmbr.guild.roles, name=role_name)
        if user_role >= mmbr.guild.me.top_role:
            self.bot.logger.warning("The Button: Cannot reach role {0.name} ({0.mention})".format(user_role))
            return
        await mmbr.add_roles(user_role, reason=self.bot.i18n.localize('button_reason', guild=mmbr.guild).format(delta))

    async def true_msg_update(self, user=None):
        dtnow = datetime.datetime.now()
        pre_reset_delta = datetime.timedelta(seconds=1)
        if user is not None:
            pre_reset_delta = self.end_game_at - dtnow  # for user score
            self.end_game_at = dtnow + datetime.timedelta(seconds=self.button_json['initial_timer'])
        time_reset_delta = self.end_game_at - dtnow
        embed_flags = {'timestamp': dtnow}
        game_sec_left = time_reset_delta.total_seconds()
        to_continue = game_sec_left > 0 and abs(pre_reset_delta.total_seconds()) >= 0  # hacky fix
        if to_continue:
            remaining = time_format(game_sec_left, self.bot.i18n)  # 2 minutes, 1.25 seconds...
            embed_flags['title'] = self.bot.i18n.localize('button_title', channel=self.the_message.channel, guild=self.the_message.guild).format(remaining)
            embed_flags['color'] = self.get_color(round(game_sec_left))[0]

            if user is not None:  # if the button was pressed, reset + add roles
                self.button_json['pressers'] += 1
                user_sec_left = pre_reset_delta.total_seconds()
                self.button_json[str(user.id)] = user_sec_left  # saves the time remaining to file
                self.write_json()
                await self.add_role(user, user_sec_left)  # adds a respective role
                self.write_message.change_interval(seconds=default_loop_interval)  # go back to normal ratelimits

            desc = [self.bot.i18n.localize("button_desc", channel=self.the_message.channel, guild=self.the_message.guild).format(self.button_json['BTN'])]
            if self.button_json['pressers'] > 0:
                desc.append(self.bot.i18n.localize("button_desc2", channel=self.the_message.channel, guild=self.the_message.guild).format(self.button_json['pressers']))
            embed_flags['description'] = '\n'.join(desc)

            if round(game_sec_left) <= 6 + default_loop_interval:  # danger zone!! uh oh
                self.write_message.change_interval(seconds=1.0, minutes=0.0, hours=0.0)
                # self.write_message.restart()
        else:
            if not self.button_json['work']:
                return
            self.button_json['work'] = False
            self.button_json['duration'] = (dtnow - self.start).total_seconds()
            print(self.button_json['duration'])
            self.write_json()
            lasted = time_format(self.button_json['duration'], self.bot.i18n)
            embed_flags['title'] = f"You lasted {lasted}."
            embed_flags['color'] = discord.Color.greyple()
            embed_flags['description'] = '\n'.join([self.bot.i18n.localize('button_inactive', channel=self.the_message.channel, guild=self.the_message.guild),
                                                    self.bot.i18n.localize("button_final", channel=self.the_message.channel, guild=self.the_message.guild).format(self.button_json['pressers'])])
            await self.the_message.clear_reaction(self.button_json['BTN'])
        embed = discord.Embed(**embed_flags)
        await self.the_message.edit(content='', embed=embed)
        if not to_continue:
            self.write_message.cancel()

    @tasks.loop(seconds=default_loop_interval)
    async def write_message(self):
        try:
            the_channel = self.bot.get_channel(self.button_json['channel_id'])
            if "message_id" not in self.button_json:
                self.the_message = await the_channel.send(self.bot.i18n.localize('button_loading', channel=the_channel, guild=the_channel.guild))
                self.button_json["message_id"] = self.the_message.id
                await self.the_message.add_reaction(self.button_json['BTN'])
                self.write_json()
            else:
                # idk if this int conversion is necessary
                self.the_message = await the_channel.fetch_message(int(self.button_json["message_id"]))
            await self.true_msg_update()
        except:
            traceback.print_exc()

    @write_message.before_loop
    async def before_write(self):
        await self.bot.wait_until_ready()

    @commands.Cog.listener()
    async def on_raw_reaction_add(self, reactpayload):
        if reactpayload.channel_id != self.button_json['channel_id']:
            return  # ignore random passing console errors from DMs and stuff
        msgid = reactpayload.message_id
        userid = reactpayload.user_id
        emoji = reactpayload.emoji
        member = reactpayload.member
        created = member.created_at
        removable_bools = [self.button_json['work'],
                           msgid == int(self.button_json["message_id"]),
                           str(emoji) == self.button_json['BTN'],
                           userid != self.bot.user.id]
        other_bool = [created < self.start, not member.bot, str(userid) not in self.button_json]
        if all(removable_bools):
            if all(other_bool):
                await self.true_msg_update(member)
            if self.the_message is None:
                msg = await self.bot.get_channel(reactpayload.channel_id).fetch_message(msgid)
            else:
                msg = self.the_message
            await msg.remove_reaction(emoji, member)


def setup(bot):
    bot.add_cog(Button(bot))
